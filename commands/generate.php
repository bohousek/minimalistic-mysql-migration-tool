<?php

$version = date('YmdHis') . sprintf("%03d", rand(0, 999));

if (sizeof($argv) < 3) {
    echo "Please specify the name of the migration: php migrate.php generate <name>\n";
    return;
}

$name = $argv[2];

for ($i=3; $i<sizeof($argv); $i++) {
    $name = $name . " " . $argv[$i];
}

function slug($string)
{
    //Unwanted:  {UPPERCASE} ; / ? : @ & = + $ , . ! ~ * ' ( )
    $string = strtolower($string);
    //Strip any unwanted characters
    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
    //Clean multiple dashes or whitespaces
    $string = preg_replace("/[\s-]+/", " ", $string);
    //Convert whitespaces and underscore to dash
    $string = preg_replace("/[\s_]/", "_", $string);
    return $string;
}

$name = slug($name);
if (!is_dir($migration_dir)) {
    try {
        if (!mkdir($migration_dir)) {
            throw new Exception("You dont have premision create a dir");
        }
    } catch (Exception $e) {
        echo "message" . $e->getMessage() . "\n";
    }
}
$migration_filename = $migration_dir . "/" . $version . "_" . $name . ".sql";


$template_content = "# automatic generated file with SQL query to migrate.
# Please add here your SQL QUERY
  
";

if (file_put_contents($migration_filename, $template_content) === false) {
    die("Cannot create the file '" . $migration_filename . "'\n");
}

chmod($migration_filename, 0777);

echo "The script '" . $migration_filename . "'.\n";
