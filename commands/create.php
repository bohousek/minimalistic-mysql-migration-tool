<?php

$result = mysqli_query($db_conn, "SHOW DATABASES;");

$exist = false;
while ($line = mysqli_fetch_assoc($result)) {
    if ($line['Database'] == $mysql_database_name) {
        $exist = true;
    }
}

mysqli_free_result($result);

if ($exist == true) {
    echo "The database '" . $mysql_database_name . "' already exists.\n";
    return;
}

mysqli_query($db_conn, "CREATE DATABASE `" . $mysql_database_name . "`;") or die("Cannot create database: " . mysqli_error() . "\n");


echo "The database '" . $mysql_database_name . "' is created.\n";
