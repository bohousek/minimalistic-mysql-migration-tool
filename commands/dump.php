<?php

echo $mysql_database_name . "\n";

$result = mysqli_query($db_conn, "SHOW TABLES;");

$dump_sql = "";


while ($line = mysqli_fetch_row($result)) {
    if ($line[0] == "schema_migrations") {
        continue;
    }

    $table_name = $line[0];

    $create_table_result = mysqli_query($db_conn, "SHOW CREATE TABLE `" . $table_name . "`;");
    $this_sql = mysqli_fetch_row($create_table_result);

    $dump_sql .= "mysqli_query(\"" . $this_sql[1] . "\");\n\n";

    mysqli_free_result($create_table_result);
}

mysqli_free_result($result);
if (!is_dir($dump_dir)) {
    try {
        if (!mkdir($dump_dir)) {
            throw new Exception("You dont have premission create a dir");
        }
    } catch (Exception $e) {
        echo "Message:" . $e->getMessage() . "\n";
    }
}
$schema_filename = $dump_dir . "/" . date('YmdHis') . "_" . $mysql_database_name . ".sql";
if (file_put_contents($schema_filename, $dump_sql) === false) {
    echo "Cannot create the file '" . $schema_filename . "'\n";
}

chmod($schema_filename, 0777);
echo "All schemas are dumped into ". $schema_filename;
