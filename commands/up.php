<?php


$migrations = getAllMigrations();

foreach ($migrations as $name) {
    echo "=== Checking " . $name . " ===\n";

    $version = substr($name, 0, 17);

    $result = mysqli_query($db_conn, "SELECT * FROM `schema_migrations` WHERE `version` = '" . $version . "'");
    $data = mysqli_fetch_assoc($result);
    mysqli_free_result($result);

    if ($data) {
        echo "\t Previously migrated. Do nothing.\n\n";
        continue;
    }

    echo "\t" . $version . " hasn't been migrated.\n";

    require $migration_dir . "/" . $name . ".sql";
    $fileQuery = file_get_contents($migration_dir . "/" . $name . ".sql");
    mysqli_query($db_conn, $fileQuery);
    mysqli_query($db_conn, "INSERT INTO `schema_migrations` (`id`, `version`) VALUES (NULL, '".$version."');");
    echo "\tMigrated.";
    echo "\n\n";
}
