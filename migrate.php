<?php
if (isset($argv[1])) {
    $cmd = $argv[1]; // $argv[0] is the file's name
} else {
    help();
}
// config

$mysql_database_name = "test_migration";
$mysql_username = "root";
$mysql_password = "deset";
$mysql_port = 3306;

$migration_dir = "migration"; //dir where migration is stored
$dump_dir = "dump"; // dir where a dump is stored

// functions

function help()
{
    echo " 
      MIGRATION TOOL\n
      Please use one of command:\n
      create: for create database\n
      generate: for generate migration\n
      up: fur up the migration\n
      dump: to dump table schema\n";
    exit();
}


function getAllMigrations()
{

    global $migration_dir;

    $migrations = array();
    $dh = opendir($migration_dir);
    while (($file = readdir($dh)) !== false) {
        if (is_file($migration_dir . "/" . $file)) {
            $migrations[] = substr($file, 0, -4);
        }
    }
    closedir($dh);

    sort($migrations);

    return $migrations;
}

function schema_migration($db_conn, $mysql_database_name)
{

    if (!mysqli_select_db($db_conn, $mysql_database_name)) {
        return;
    }

    $result = mysqli_query($db_conn, "SHOW TABLES;");

    $exist = false;
    while ($line = mysqli_fetch_assoc($result)) {
        foreach ($line as $k => $v) {
            if ($v == "schema_migrations") {
                $exist = true;
            }
        }
    }

    mysqli_free_result($result);

    if ($exist == true) {
        return;
    }

    mysqli_query($db_conn, "CREATE TABLE `schema_migrations` (
            `id` INT(10) AUTO_INCREMENT PRIMARY KEY,
            `version` VARCHAR(255)
            )
          ") or die("Cannot create the table 'schema_migration': " . mysqli_error() . "\n");
}





if (file_exists('commands/' . $cmd . '.php')) {
    // Connecting, selecting database
    $db_conn = mysqli_connect('127.0.0.1:'.$mysql_port, $mysql_username, $mysql_password)
                or die('Could not connect Mysql: ' . mysqli_error() . "\n");

    schema_migration($db_conn, $mysql_database_name);
    require 'commands/' . $cmd . '.php';

    mysqli_close($db_conn);
}

    echo "\r\n";
    

    exit(0);
